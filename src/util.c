#include "cest/util.h"
#include <stdlib.h>
#include <string.h>

int cest_html_encode(FILE* dst, FILE* src) {
    char c;

    while (!feof(src)) {
        c = fgetc(src);
        if (feof(src)) break;
        switch (c) {
            case '\0': break;
            case '&': fprintf(dst, "&amp;"); break;
            case '<': fprintf(dst, "&lt;"); break;
            case '>': fprintf(dst, "&gt;"); break;
            case '\'': fprintf(dst, "&apos;"); break;
            case '"': fprintf(dst, "&quot;"); break;
            default: fputc(c, dst); break;
        }
    }
    return 0;
}

int cest_url_encode(FILE* dst, FILE* src) {
    char c;

    while (!feof(src)) {
        c = fgetc(src);
        if (feof(src)) break;
        if ((c >= 'a' && c <= 'z') ||
            (c >= 'A' && c <= 'Z') ||
            (c >= '0' && c <= '9') ||
            (c == '_') ||
            (c == '-'))
            fputc(c, dst);
        else
            fprintf(dst, "%02x", (int)(c));
    }
    return 0;
}

inline int fgethex(FILE* f, size_t count) {
    int accum = 0;
    int b = 0;
    while (count--) {
        accum <<= 4;
        b = fgetc(f);
        if (feof(f)) return -2;
        if (b >= 'a' && b <= 'f') {
            accum += b + 10 - 'a';
        }
        else if (b >= 'A' && b <= 'F') {
            accum += b + 10 - 'A';
        }
        else if (b >= '0' && b <= '9') {
            accum += b - '0';
        }
        else {
            // fail
            return -1;
        }
    }
    return accum;
}

int cest_url_decode(FILE* dst, FILE* src) {
    char c;

    while (!feof(src)) {
        c = fgetc(src);
        if (feof(src)) break;
        switch (c) {
            case '+':
                fputc(' ', dst);
                break;
            case '%':
                c = fgethex(src, 2);
                if (c < 0) return -1;
                fputc(c, dst);
                break;
            default:
                fputc(c, dst);
                break;
        }
    }
    return 0;
}

char* cest_run_stream_converter(int (*convert)(FILE*, FILE*), const char* src, size_t src_len, size_t *dst_len) {
    char* dst = NULL;
    FILE* dstf = open_memstream(&dst, dst_len);
    int result;

    result = cest_write_stream_converter(convert, dstf, src, src_len);
    fclose(dstf);
    if (result) {
        free(dst);
        *dst_len = 0;
        return NULL;
    }
    else {
        return dst;
    }
}

int cest_write_stream_converter(int (*convert)(FILE*, FILE*), FILE* dstf, const char* src, size_t src_len) {
    FILE* srcf = NULL;
    int result;

    srcf = fmemopen((void*)src, src_len, "r");
    result = convert(dstf, srcf);
    fclose(srcf);
    return result;
}
