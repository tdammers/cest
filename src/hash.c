#include "cest/internal/hash.h"
#include <stdio.h>
#include <string.h>

#include "cest/internal/bobjenkins-lookup3.h"


void _cest_internal_seed_hash(char* dst, size_t num_bytes) {
    FILE* f = NULL;

    f = fopen("/dev/urandom", "rb");
    fread(dst, num_bytes, 1, f);
    fclose(f);
}

#define _seed_length 32

int _cest_internal_hash(const char* k) {
    static char seed[_seed_length] = { '\0' };
    static int seed_initialized = 0;
    static uint32_t seed_hash = 0;

    if (!seed_initialized) {
        _cest_internal_seed_hash(seed, _seed_length);
        seed_hash = hashlittle(seed, _seed_length, 0);
        seed_initialized = 1;
    }

    if (k)
        return hashlittle(k, strlen(k), seed_hash);
    else
        return seed_hash;
}


