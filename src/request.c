#include "cest/request.h"

#include "cest/headers.h"
#include "cest/vector.h"
#include "cest/dictionary.h"
#include "cest/util.h"

#include <assert.h>
#include <fcntl.h>
#include <malloc.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define _REQUEST_MAX_LINE_LENGTH 1024

struct cest_request {
    cest_request_method_t method;
    char* path;
    cest_headers_t* headers;
    char* body;
    size_t body_length;
    FILE* body_stream;
    cest_dictionary_t* get_params;
};

cest_request_t* cest_create_request() {
    cest_request_t* rq = malloc(sizeof(cest_request_t));
    rq->method = CEST_GET;
    rq->path = NULL;
    rq->headers = cest_create_headers();
    rq->body = NULL;
    rq->body_length = 0;
    rq->body_stream = NULL;
    rq->get_params = NULL;
    return rq;
}

void cest_destroy_request(cest_request_t* rq) {
    assert(rq);
    cest_destroy_headers(rq->headers);
    free(rq->body);
    free(rq->path);
    if (rq->get_params) {
        cest_destroy_dictionary(rq->get_params);
    }
    free(rq);
}

void cest_set_request_method(cest_request_t* rq, cest_request_method_t method) {
    assert(rq);
    rq->method = method;
}

void cest_set_request_path(cest_request_t* rq, const char* path) {
    assert(rq);
    free(rq->path);
    rq->path = strdup(path);
}

void cest_add_request_header(cest_request_t* rq, const char* name, const char* value) {
    assert(rq);
    cest_add_header(rq->headers, name, value);
}

void cest_append_request_body(cest_request_t* rq, const char* str, size_t str_len) {
    rq->body = realloc(rq->body, rq->body_length + str_len);
    memcpy(rq->body + rq->body_length, str, str_len);
    rq->body_length += str_len;
}

cest_request_method_t cest_get_request_method(const cest_request_t* rq) {
    assert(rq);
    return rq->method;
}

const char* cest_get_request_path(const cest_request_t* rq) {
    assert(rq);
    return rq->path;
}

void cest_load_request_body(cest_request_t* rq) {
    assert(rq);
    if (rq->body_stream) {
        const char* content_length_str = cest_get_request_header(rq, "Content-Length");
        if (content_length_str) {
            size_t content_length = atoi(content_length_str);
            size_t bytes_read = 0;
            char buffer[content_length+1];
            buffer[content_length] = '\0';
            bytes_read = fread(buffer, 1, content_length, rq->body_stream);
            if (bytes_read < 0) {
                printf("fread returned < 0");
                exit(-1);
            }
            else {
                cest_append_request_body(rq, buffer, bytes_read);
            }
        }
    }
    rq->body_stream = NULL;
}

const char* cest_get_request_body(const cest_request_t* rq) {
    assert(rq);
    return rq->body;
}

const char* cest_get_full_request_body(cest_request_t* rq) {
    cest_load_request_body(rq);
    return cest_get_request_body(rq);
}

size_t cest_get_request_body_length(const cest_request_t* rq) {
    assert(rq);
    return rq->body_length;
}

size_t cest_get_full_request_body_length(cest_request_t* rq) {
    cest_load_request_body(rq);
    return cest_get_request_body_length(rq);
}

void cest_set_request_header(cest_request_t* rq, const char* name, const char* value) {
    cest_set_header(rq->headers, name, value);
}

const cest_headers_t* cest_get_request_headers(const cest_request_t* rq) {
    assert(rq);
    return rq->headers;
}

const char* cest_get_request_header(const cest_request_t* rq, const char* name) {
    assert(rq);
    return cest_get_header(cest_get_request_headers(rq), name);
}

const char* cest_request_method_name(cest_request_method_t m) {
    switch (m) {
        case CEST_HEAD: return "HEAD";
        case CEST_GET: return "GET";
        case CEST_OPTIONS: return "OPTIONS";
        case CEST_POST: return "POST";
        case CEST_PUT: return "PUT";
        case CEST_DELETE: return "DELETE";
        default: return NULL;
    }
}

void cest_set_request_body_stream(cest_request_t* rq, FILE* f) {
    if (rq->body_stream) {
        fclose(rq->body_stream);
    }
    rq->body_stream = f;
}

char* cest_read_line(FILE* f) {
    char c;
    struct {
        char* items;
        size_t cap;
        size_t len;
    } buffer;
    vector_init(char, buffer);
    while (!feof(f)) {
        c = fgetc(f);
        if (c == '\n' || c == 0) {
            vector_push(char, buffer, '\0');
            break;
        }
        if (c == '\r') {
            c = fgetc(f);
            if (c == '\n') {
                vector_push(char, buffer, '\0');
                break;
            }
            else {
                printf("Expected \\n character after \\r, but found '%c'\n", c);
                exit(-1);
            }
        }
        else {
            vector_push(char, buffer, c);
        }
    }
    return buffer.items;
}

cest_request_t* cest_parse_request_fd(int fd) {
    FILE* f = fdopen(fd, "r");
    cest_request_t* rq;

    if (!f) {
        printf(" ERROR: could not open request stream.\n");
        return cest_create_request();
    }
    rq = cest_parse_request(f);
    fclose(f);
    return rq;
}

typedef struct method_mapping {
    const char* str;
    cest_request_method_t method;
} method_mapping_t;

static method_mapping_t method_mappings[] = {
    { "GET", CEST_GET },
    { "PUT", CEST_PUT },
    { "POST", CEST_POST },
    { "DELETE", CEST_DELETE },
    { "HEAD", CEST_HEAD },
    { "OPTIONS", CEST_OPTIONS },
    { NULL, CEST_GET } // sentinel & default
};

cest_request_method_t _parse_request_method(const char* str) {
    method_mapping_t* mapping;

    for (mapping = method_mappings; mapping->str; ++mapping) {
        if (strcmp(mapping->str, str) == 0)
            return mapping->method;
    }

    printf("Warning: unknown request method '%s'\n", str);

    return mapping->method;
}

void _parse_http_line(FILE* f, cest_request_t* rq) {
    char* ln = cest_read_line(f);
    char* saveptr = NULL;
    char* delim = " \t";
    char* http_method_str = strtok_r(ln, delim, &saveptr);
    char* http_path_str = strtok_r(NULL, delim, &saveptr);
    char* http_version_str = strtok_r(NULL, delim, &saveptr);

    cest_request_method_t method = _parse_request_method(http_method_str);

    if (strcmp(http_version_str, "HTTP/1.1") != 0) {
        printf("Invalid HTTP version specifier: %s\n", http_version_str);
        exit(-5);
    }

    cest_set_request_method(rq, method);
    cest_set_request_path(rq, http_path_str);
    free(ln);
}

void _parse_headers(FILE* f, cest_request_t* rq) {
    while (!feof(f)) {
        char* ln = cest_read_line(f);
        char* saveptr = NULL;
        char* delim = ":";
        char* name = strtok_r(ln, delim, &saveptr);
        char* value = strtok_r(NULL, delim, &saveptr);

        if (!name) {
            return;
        }
        // Skip leading whitespace at beginning of header value
        while (*value && *value <= 32) {
            ++value;
        }
        cest_set_request_header(rq, name, value);
    }
}

void _parse_get_params(cest_request_t* rq) {
    if (rq->get_params) return; // already parsed

    char* params = strchr(rq->path, '?');
    rq->get_params = cest_create_dictionary();
    if (!params) return;
    params = strdup(params + 1);
    char* name = params;
    char* found = params;
    char* value;
    while (*name) {
        value = strchr(name, '=');
        found = strchr(name, '&');
        if (!found) {
            found = strchr(name, '\0');
        }
        if (!value) {
            value = strchr(name, '\0');
        }
        if (found < value) {
            *found = '\0';
            ++found;
            cest_dictionary_insert(rq->get_params, name, "");
        }
        else {
            char* decoded_value = NULL;
            size_t decoded_len = 0;
            *found = '\0';
            ++found;
            *value = '\0';
            ++value;
            decoded_value = cest_run_stream_converter(cest_url_decode, value, strlen(value), &decoded_len);
            cest_dictionary_insert(rq->get_params, name, decoded_value);
        }
        name = found;
    }
}

const char* cest_get(cest_request_t* rq, const char* key) {
    _parse_get_params(rq);
    return cest_dictionary_lookup(rq->get_params, key);
}

cest_request_t* cest_parse_request(FILE* f) {
    cest_request_t* rq = cest_create_request();

    _parse_http_line(f, rq);
    _parse_headers(f, rq);
    cest_set_request_body_stream(rq, f);

    return rq;
}
