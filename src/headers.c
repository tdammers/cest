#include "cest/headers.h"
#include "cest/vector.h"
#include <memory.h>
#include <malloc.h>
#include <assert.h>

struct cest_header {
    char* name;
    char* value;
};

vector_of(cest_header_t, cest_headers);

cest_header_t* cest_create_header(const char* name, const char* value) {
    cest_header_t* header = malloc(sizeof(cest_header_t));
    header->name = strdup(name);
    header->value = strdup(value);
    return header;
}

void cest_destroy_header(cest_header_t* header) {
    free(header->name);
    free(header->value);
    free(header);
}

const char* cest_get_header_name(const cest_header_t* header) { return header->name; }
const char* cest_get_header_value(const cest_header_t* header) { return header->value; }

cest_headers_t* cest_create_headers() {
    cest_headers_t* headers = malloc(sizeof(cest_headers_t));
    vector_init(cest_header_t, *headers);
    return headers;
}

const char* cest_get_header(const cest_headers_t* headers, const char* name) {
    size_t i = 0;
    for (i = 0; i < headers->len; ++i) {
        if (strcasecmp(headers->items[i]->name, name) == 0) {
            return headers->items[i]->value;
        }
    }
    return NULL;
}

void cest_set_header(cest_headers_t* headers, const char* name, const char* value) {
    size_t i = 0;
    for (i = 0; i < headers->len; ++i) {
        if (strcasecmp(headers->items[i]->name, name) == 0) {
            free(headers->items[i]->value);
            headers->items[i]->value = strdup(value);
            return;
        }
    }
    cest_add_header(headers, name, value);
}

void cest_add_header(cest_headers_t* headers, const char* name, const char* value) {
    cest_header_t* header = cest_create_header(name, value);
    vector_push(cest_header_t, *headers, header);
}

void cest_destroy_headers(cest_headers_t* headers) {
    assert(headers);
    vector_delete(cest_header_t, *headers, cest_destroy_header);
    free(headers);
}

size_t cest_get_num_headers(const cest_headers_t* headers) {
    assert(headers);
    return headers->len;
}

cest_header_t* const* cest_get_headers(const cest_headers_t* headers) {
    assert(headers);
    return headers->items;
}
