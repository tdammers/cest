#include "cest/response.h"
#include "cest/headers.h"
#include "cest/vector.h"
#include <memory.h>
#include <malloc.h>
#include <assert.h>

typedef struct _body_writer {
    void (*func)(FILE* outstream, void *data);
    void (*dtor)(void *data);
    void *data;
    size_t content_length;
} _body_writer_t;

void _destroy_body_writer(_body_writer_t* w) {
    if (w->dtor) {
        w->dtor(w->data);
    }
    free(w);
}

typedef struct _body_writers {
    _body_writer_t** items;
    size_t cap;
    size_t len;
} _body_writers_t;

struct cest_response {
    int status;
    cest_headers_t* headers;
    _body_writers_t body_writers;
};

cest_response_t* cest_create_response() {
    cest_response_t* rp = malloc(sizeof(cest_response_t));
    rp->status = 200;
    rp->headers = cest_create_headers();
    vector_init(_body_writer_t, rp->body_writers);
    return rp;
}

void cest_destroy_response(cest_response_t* rp) {
    cest_destroy_headers(rp->headers);
    vector_delete(_body_writer_t, rp->body_writers, _destroy_body_writer);
    free(rp);
}

void cest_set_response_status(cest_response_t* rp, int status) {
    assert(status >= 100);
    assert(status < 600);
    assert(rp);
    rp->status = status;
}

int cest_get_response_status(const cest_response_t* rp) {
    assert(rp);
    return rp->status;
}

void cest_set_response_header(cest_response_t* rp, const char* name, const char* value) {
    cest_set_header(rp->headers, name, value);
}

const cest_headers_t* cest_get_response_headers(const cest_response_t* rp) {
    assert(rp);
    return rp->headers;
}

int cest_get_response_body_length(const cest_response_t* rp) {
    size_t i;
    int body_length = 0;

    assert(rp);
    for (i = 0; i < rp->body_writers.len; ++i) {
        body_length += rp->body_writers.items[i]->content_length;
    }

    return body_length;
}

void cest_reset_content_length_header(cest_response_t* rp) {
    char buf[1024];
    snprintf(buf, 1024, "%i", cest_get_response_body_length(rp));
    cest_set_response_header(rp, "Content-Length", buf);
}

void cest_clear_response_body(cest_response_t* rp) {
    vector_delete(_body_writer_t, rp->body_writers, _destroy_body_writer);
    vector_init(_body_writer_t, rp->body_writers);
}

void _add_body_writer(cest_response_t* rp, _body_writer_t* writer) {
    vector_push(_body_writer_t, rp->body_writers, writer);
}

typedef struct _string_body_writer_data {
    char* str;
    size_t len;
} _string_body_writer_data_t;

void _string_body_writer(FILE* stream, void *data) {
    _string_body_writer_data_t* wdata = data;
    fwrite(wdata->str, 1, wdata->len, stream);
}

void _string_body_writer_dtor(void *data) {
    _string_body_writer_data_t* wdata = data;
    free(wdata->str);
    free(wdata);
}

typedef struct _stream_body_writer_data {
    FILE* stream;
    size_t len;
    int auto_close;
} _stream_body_writer_data_t;

void _stream_body_writer(FILE* dst, void *data) {
    char buffer[1024];
    size_t bytes_read = 0;
    size_t bytes_read_total = 0;
    size_t bytes_to_read = 0;
    _stream_body_writer_data_t* wdata = data;
    FILE* src = wdata->stream;

    rewind(wdata->stream);
    while (!feof(wdata->stream) && bytes_read_total < wdata->len) {
        bytes_to_read = wdata->len - bytes_read_total;
        if (bytes_to_read > 1024)
            bytes_to_read = 1024;
        bytes_read = fread(buffer, 1, bytes_to_read, src);
        bytes_read_total += bytes_read;
        fwrite(buffer, 1, bytes_read, dst);
    }
}

void _stream_body_writer_dtor(void *data) {
    _stream_body_writer_data_t* wdata = data;
    if (wdata->auto_close) {
        fclose(wdata->stream);
    }
    free(wdata);
}


void cest_set_response_body(cest_response_t* rp, const char* str, size_t str_len) {
    cest_clear_response_body(rp);
    cest_append_response_body(rp, str, str_len);
}

void cest_append_response_body_callback(cest_response_t* rp, void (*callback)(FILE*, void*), void (*cleanup)(void*), void *data, size_t content_length) {
    _body_writer_t* w = malloc(sizeof(_body_writer_t));
    w->func = callback;
    w->dtor = cleanup;
    w->data = data;
    w->content_length = content_length;
    _add_body_writer(rp, w);
}

void cest_append_response_body(cest_response_t* rp, const char* str, size_t str_len) {
    _string_body_writer_data_t *d = malloc(sizeof(_string_body_writer_data_t));
    d->str = strdup(str);
    d->len = str_len;
    cest_append_response_body_callback(rp,
        _string_body_writer,
        _string_body_writer_dtor,
        d, str_len);
}

void cest_append_response_body_stream(cest_response_t* rp, FILE* stream, size_t len, int auto_close) {
    _stream_body_writer_data_t *d = malloc(sizeof(_stream_body_writer_data_t));
    d->stream = stream;
    d->len = len;
    d->auto_close = auto_close;
    cest_append_response_body_callback(rp,
        _stream_body_writer,
        _stream_body_writer_dtor,
        d, len);
}

void cest_write_response_fd(int fd, const cest_response_t* rp) {
    FILE* f = fdopen(fd, "w");
    cest_write_response(f, rp);
    fclose(f);
}

const char* _get_status_message(int status) {
    switch (status) {
        case 200: return "OK";
        case 302: return "Found";
        case 400: return "Client Error";
        case 403: return "Forbidden";
        case 404: return "Not Found";
        case 405: return "Not Allowed";
        case 500: return "Server Error";
        default: return "Unknown Status";
    }
}

void _write_status_line(FILE* f, const cest_response_t* rp) {
    int status = cest_get_response_status(rp);
    fprintf(f, "HTTP/1.1 %i %s\r\n",
        status, _get_status_message(status));
}

void _write_headers(FILE* f, const cest_response_t* rp) {
    size_t i;
    const cest_headers_t* response_headers = cest_get_response_headers(rp);
    size_t num_headers = cest_get_num_headers(response_headers);
    cest_header_t* const* headers = cest_get_headers(response_headers);

    for (i = 0; i < num_headers; ++i) {
        const cest_header_t* header = headers[i];
        fprintf(f, "%s: %s\r\n",
            cest_get_header_name(header),
            cest_get_header_value(header));
    }
    fprintf(f, "\r\n");
}

void _write_body(FILE* f, const cest_response_t* rp) {
    size_t i;
    for (i = 0; i < rp->body_writers.len; ++i) {
        _body_writer_t* w = rp->body_writers.items[i];
        w->func(f, w->data);
    }
}

void cest_write_response(FILE* f, const cest_response_t* rp) {
    _write_status_line(f, rp);
    _write_headers(f, rp);
    _write_body(f, rp);
}
