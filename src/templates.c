#include "cest/templates.h"

#include <stdarg.h>
#include <string.h>
#include "cest/util.h"

typedef void (*cest_template_thunk_t)(FILE* , void*);

void _write_template(FILE* f, va_list *vl, int in_tag) {
    do {
        const char* str = va_arg(*vl, const char*);
        if (!str) {
            return;
        }
        if (in_tag && str[0] != '@') {
            fprintf(f, ">");
            in_tag = 0;
        }
        switch (str[0]) {
            // Tag open or tag close
            case '<':
                if (str[1] == '/') {
                    return; // pop
                }
                else {
                    fprintf(f, "<%s", str + 1);
                    _write_template(f, vl, 1); // push
                    fprintf(f, "</%s>", str + 1);
                }
                break;

            // Attribute
            case '@':
                if (in_tag) {
                    const char* value;
                    fprintf(f, " ");
                    cest_write_stream_converter(
                        cest_html_encode,
                        f, str + 1, strlen(str + 1));
                    fprintf(f, "=\"");
                    value = va_arg(*vl, const char*);
                    cest_write_stream_converter(
                        cest_html_encode,
                        f, value, strlen(value));
                    fprintf(f, "\"");
                }
                else {
                    // ignore
                    va_arg(*vl, const char*);
                }
                break;

            // Raw HTML (unescaped)
            case '!': {
                    const char* raw_html;
                    raw_html = va_arg(*vl, const char*);
                    fprintf(f, "%s", raw_html);
                }
                break;

            // Call a thunk
            case '$': {
                    cest_template_thunk_t thunk = va_arg(*vl, cest_template_thunk_t);
                    void *thunk_arg = va_arg(*vl, void*);
                    thunk(f, thunk_arg);
                }
                break;

            // Literal string -> escaped text
            default:
                cest_write_stream_converter(cest_html_encode, f, str, strlen(str));
                break;

        }
    } while (1);
}

void cest_write_template(FILE* f, ...) {
    va_list vl;

    va_start(vl, f);
    _write_template(f, &vl, 0);
    va_end(vl);
}

