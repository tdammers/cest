#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

#include "cest/handler.h"
#include "cest/request.h"
#include "cest/response.h"

#define error(msg, retval) do { perror(msg); return retval; } while (0)

int cest_serve(int port, cest_handler_t handler, void* user_state) {
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr;
    int optval = 1;

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    if (listenfd < 0) {
        error("Could not create listener socket", -2);
    }


    if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int)) < 0) {
        error("Could not set SO_REUSEADDR", -10);
    }
    if (setsockopt(listenfd, SOL_SOCKET, SO_KEEPALIVE, &optval, sizeof(int)) < 0) {
        error("Could not set SO_KEEPALIVE", -11);
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(port);

    if (bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr))) {
        error("Could not bind to listener socket", -3);
    }

    if (listen(listenfd, 10)) {
        error("Could not start listening", -4);
    }

    printf("Listening on port %i...\n", port);

    while(1)
    {
        int child_pid;
        struct sockaddr client_addr;
        socklen_t client_len = sizeof(struct sockaddr);

        connfd = accept(listenfd, &client_addr, &client_len);
        child_pid = fork();
        if (child_pid < 0) {
            error("Failed to fork", -5);
        }
        if (child_pid) {
            int status;
            close(connfd);
            waitpid(child_pid, &status, 0);
        }
        else {
            int grandchild_pid;

            grandchild_pid = fork();
            if (grandchild_pid < 0) {
                error("Failed to double-fork", -6);
            }
            if (grandchild_pid) {
                close(connfd);
                exit(0);
            }
            else {
                cest_request_t* rq;
                cest_response_t* rp;
                FILE* connf;

                connf = fdopen(connfd, "r+");
                rq = cest_parse_request(connf);
                rewind(connf);
                rp = cest_create_response();

                handler(rq, rp, user_state);

                if (cest_get_request_method(rq) == CEST_HEAD) {
                    cest_clear_response_body(rp);
                }

                cest_write_response(connf, rp);

                cest_destroy_request(rq);
                cest_destroy_response(rp);

                fsync(connfd);
                fclose(connf);
                close(connfd);
                exit(0);
            }
        }
     }
     return 0;
}

