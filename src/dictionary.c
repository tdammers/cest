#include "cest/dictionary.h"
#include "cest/internal/hash.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "cest/vector.h"
#include <math.h>

float _max_load_factor = 2.5f;
float _min_load_factor = 0.5f;

typedef struct _entry {
    char* key;
    char* value;
    struct _entry *next;
} _entry_t;

typedef struct _bucket {
    _entry_t* head;
    _entry_t* last;
} _bucket_t;

_entry_t* _create_entry(const char* key, const char* value) {
    _entry_t *e = malloc(sizeof(_entry_t));
    e->key = strdup(key);
    e->value = strdup(value);
    e->next = NULL;
    return e;
}

void _destroy_entry(_entry_t* e) {
    if (!e)
        return;
    free(e->key);
    free(e->value);
    free(e);
}

void _bucket_append_entry(_bucket_t* bucket, _entry_t* new_entry) {
    assert(new_entry);
    assert(bucket);
    assert(new_entry->key);
    if (bucket->last) {
        bucket->last->next = new_entry;
        bucket->last = new_entry;
    }
    else {
        bucket->head = bucket->last = new_entry;
    }
}

void _bucket_append(_bucket_t* bucket, const char* key, const char* value) {
    assert(bucket);
    assert(value);
    _entry_t* new_entry = _create_entry(key, value);
    _bucket_append_entry(bucket, new_entry);
}

_entry_t* _bucket_shift(_bucket_t* bucket) {
    if (bucket->head) {
        _entry_t* e = bucket->head;
        bucket->head = e->next;
        if (bucket->head == NULL)
            bucket->last = NULL;
        e->next = NULL;
        return e;
    }
    else {
        return NULL;
    }
}

_entry_t* _bucket_lookup(_bucket_t* bucket, const char* key) {
    _entry_t* e;

    if (!bucket)
        return NULL;

    if (key == NULL)
        key = "";

    // Empty bucket does not contain key
    if (!bucket->head)
        return NULL;

    for (e = bucket->head; e; e = e->next) {
        assert(e->key);
        if (strcmp(e->key, key) == 0) {
            return e;
        }
    }
    return NULL;
}

size_t _bucket_remove(_bucket_t* bucket, const char* key) {
    _entry_t *e, *en;
    size_t items_removed = 0;

    while (bucket->head && (strcmp(bucket->head->key, key) == 0)) {
        e = bucket->head;
        ++items_removed;
        bucket->head = e->next;
        _destroy_entry(e);
        if (!bucket->head) {
            bucket->last = NULL;
            return items_removed;
        }
    }

    for (e = bucket->head; e->next; e = e->next) {
        while (strcmp(e->next->key, key) == 0) {
            en = e->next;
            e->next = en->next;
            ++items_removed;
            _destroy_entry(en);
            if (!e->next) {
                bucket->last = e;
                return items_removed;
            }
        }
    }

    return items_removed;
}

void _bucket_clear(_bucket_t* bucket) {
    _entry_t* e;
    while (bucket->head) {
        e = bucket->head;
        bucket->head = bucket->head->next;
        _destroy_entry(e);
    }
}

struct cest_dictionary {
    size_t hash_depth;
    size_t num_entries;
    _bucket_t* buckets;
};

void _init_dictionary(cest_dictionary_t* d, size_t depth) {
    memset(d, 0, sizeof(cest_dictionary_t));
    d->hash_depth = depth;
    d->buckets = malloc(sizeof(_bucket_t) << depth);
    memset(d->buckets, 0, sizeof(_bucket_t) << depth);
}

int _get_bucket_index(const cest_dictionary_t* d, const char* key) {
    assert(d);
    int hash = _cest_internal_hash(key);
    int mask = (1 << d->hash_depth) - 1;
    return hash & mask;
}

void _insert_entry(cest_dictionary_t* d, _entry_t* e) {
    int index;

    assert(d);
    assert(e);
    assert(e->key);
    index = _get_bucket_index(d, e->key);
    d->num_entries++;
    _bucket_append_entry(d->buckets + index, e);
}

void _resize_dictionary(cest_dictionary_t* d, size_t new_depth) {
    size_t old_depth = d->hash_depth;
    size_t b;
    if (new_depth == old_depth) {
        return;
    }
    _entry_t* e;
    _bucket_t* old_buckets = d->buckets;
    _init_dictionary(d, new_depth);
    for (b = 0; b < (1 << old_depth); ++b) {
        while ((e = _bucket_shift(old_buckets + b))) {
            _insert_entry(d, e);
        }
    }
    free(old_buckets);
}

void _resize_dictionary_to(cest_dictionary_t* d, size_t target_size) {
    size_t depth = d->hash_depth;
    size_t cap = cest_dictionary_bucket_count(d);
    size_t min_size = (int)ceil(_max_load_factor * (float)target_size);
    size_t max_size = (int)floor(_min_load_factor * (float)target_size);
    while (depth && cap > min_size) {
        cap >>= 1;
        --depth;
    }
    while (cap < max_size && cap > 1) {
        cap <<= 1;
        ++depth;
    }
    _resize_dictionary(d, depth);
}

cest_dictionary_t* cest_create_dictionary() {
    cest_dictionary_t* d = malloc(sizeof(cest_dictionary_t));
    _init_dictionary(d, 0);
    return d;
}

void cest_destroy_dictionary(cest_dictionary_t* d) {
    size_t i;
    for (i = 0; i < (1 << d->hash_depth); ++i) {
        _bucket_clear(d->buckets + i);
    }
    free(d->buckets);
    free(d);
}

const _entry_t* _find_entry_const(const cest_dictionary_t* d, const char* key) {
    assert(d);
    int index = _get_bucket_index(d, key);
    return _bucket_lookup(d->buckets + index, key);
}

_entry_t* _find_entry(cest_dictionary_t* d, const char* key) {
    assert(d);
    int index = _get_bucket_index(d, key);
    return _bucket_lookup(d->buckets + index, key);
}

void cest_dictionary_insert(cest_dictionary_t* d, const char* key, const char* value) {
    assert(d);
    _entry_t* e = _find_entry(d, key);
    if (e) {
        free(e->value);
        e->value = strdup(value);
    }
    else {
        _resize_dictionary_to(d, cest_dictionary_size(d) + 1);
        _insert_entry(d, _create_entry(key, value));
    }
}

void cest_dictionary_remove(cest_dictionary_t* d, const char* key) {
    int hash = _cest_internal_hash(key);
    int mask = (1 << d->hash_depth) - 1;
    int index = hash & mask;

    d->num_entries -= _bucket_remove(d->buckets + index, key);
    _resize_dictionary_to(d, d->num_entries);
}

const char* cest_dictionary_lookup(const cest_dictionary_t* d, const char* key) {
    assert(d);
    const _entry_t* e;

    e = _find_entry_const(d, key);
    if (e)
        return e->value;
    else
        return NULL;
}

int cest_dictionary_has(const cest_dictionary_t* d, const char* key) {
    return (cest_dictionary_lookup(d, key) != NULL);
}

size_t cest_dictionary_size(const cest_dictionary_t* d) {
    return d->num_entries;
}

size_t cest_dictionary_bucket_count(const cest_dictionary_t* d) {
    return (1 << d->hash_depth);
}
