#include "cest/middleware.h"

#include <stdlib.h>

#include "cest/vector.h"

typedef struct cest_middleware {
    cest_before_handler_f before;
    cest_after_handler_f after;
    void *state;
    void (*state_dtor)(void*);
} cest_middleware_t;

cest_middleware_t* cest_create_middleware(
    cest_before_handler_f before,
    cest_after_handler_f after,
    void* state,
    void (*state_dtor)(void*)) {

    cest_middleware_t* middleware = malloc(sizeof(cest_middleware_t));
    memset(middleware, 0, sizeof(cest_middleware_t));

    middleware->before = before;
    middleware->after = after;
    middleware->state = state;
    middleware->state_dtor = state_dtor;

    return middleware;
}

void cest_destroy_middleware(cest_middleware_t* middleware) {
    if (middleware->state_dtor) {
        middleware->state_dtor(middleware->state);
    }
    free(middleware);
}

typedef vector_of(cest_middleware_t, cest_middlewares) cest_middlewares_t;

typedef struct cest_middleware_handler {
    cest_middlewares_t middlewares;
    cest_handler_t handler;
    void* handler_state;
} cest_middleware_handler_t;

cest_middleware_handler_t* cest_create_middleware_handler(cest_handler_t handler, void* handler_state) {
    cest_middleware_handler_t* mwh = malloc(sizeof(cest_middleware_handler_t));
    memset(mwh, 0, sizeof(cest_middleware_handler_t));
    vector_init(cest_middleware_t, mwh->middlewares);
    mwh->handler = handler;
    mwh->handler_state = handler_state;
    return mwh;
}

void cest_destroy_middleware_handler(cest_middleware_handler_t* mwh) {
    vector_delete(cest_middleware_t, mwh->middlewares, cest_destroy_middleware);
    free(mwh);
}

void cest_append_middleware(cest_middleware_handler_t* mwh, cest_middleware_t* m) {
    vector_push(cest_middleware_t, mwh->middlewares, m);
}

void cest_middleware_handler(cest_request_t* rq, cest_response_t* rp, void* data) {
    size_t i = 0;

    cest_middleware_handler_t* mwh = data;

    // first middleware gets first dibs on the request
    while (i < mwh->middlewares.len) {
        cest_middleware_t* m = mwh->middlewares.items[i++];
        if (m->before) {
            if (m->before(rq, rp, m->state)) {
                // middleware reports failure: abort processing.
                return;
            }
        }
    }

    mwh->handler(rq, rp, mwh->handler_state);

    // first middleware gets last dibs on response
    while (i-- > 0) {
        cest_middleware_t* m = mwh->middlewares.items[i];
        if (m->after) {
            if (m->after(rq, rp, m->state)) {
                // middleware reports failure: abort processing.
                return;
            }
        }
    }
}
