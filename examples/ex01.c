#include <stdio.h>
#include <stdlib.h>
#include "cest/handler.h"
#include "cest/server.h"
#include "cest/request.h"
#include "cest/templates.h"

void master_template(FILE* f, const char* title, cest_template_thunk_t thunk, void* thunk_data) {
    return cest_write_template(f,
        "!",
        "<!DOCTYPE html>",
        "<html",
            "<head",
            "</",
            "<body",
                "<h1",
                    title,
                "</",
                "<div",
                    "@class", "content",
                    "$", thunk, thunk_data,
                "</",
            "</",
        "</",
        NULL);
}

void hello_template(FILE* f, void* data) {
    cest_request_t* rq = data;
    cest_request_method_t method = cest_get_request_method(rq);
    const char* path = cest_get_request_path(rq);
    const char* name = cest_get(rq, "name");
    if (!name)
        name = "unknown visitor";

    cest_write_template(f,
        "<p",
            "Hello, ",
            name,
        "</",
        "<p",
            "@class", "request-info",
            "<strong",
                "You issued a ",
                cest_request_method_name(method),
                " request to the following path:",
            "</",
            "<pre", path, "</",
        "</",
        NULL);
}

void handler(cest_request_t* rq, cest_response_t* rp, void* user_state) {
    char* response_body = NULL;
    size_t response_length = 0;
    FILE* response_stream = open_memstream(&response_body, &response_length);

    cest_load_request_body(rq);
    cest_set_response_header(rp, "Content-Type", "text/html");
    master_template(response_stream,
        "Welcome To CEST",
        hello_template, (void*)rq);
    fclose(response_stream);
    cest_append_response_body(rp, response_body, response_length);
    free(response_body);
}

int main(int argc, char *argv[])
{
    int port = 0;

    if (argc < 2) {
        perror("Please provide a port number");
        exit(-1);
    }
    port = atoi(argv[1]);
    return cest_serve(port, *handler, NULL);
}
