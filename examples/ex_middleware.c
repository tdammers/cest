#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cest/handler.h"
#include "cest/server.h"
#include "cest/request.h"
#include "cest/middleware.h"

void handler(cest_request_t* rq, cest_response_t* rp, void* data) {
    const char* path = cest_get_request_path(rq);

    cest_set_response_header(rp, "Content-Type", "text/plain");

    if (strcmp(path, "/") == 0) {
        cest_append_response_body(rp, "Hello, world!", strlen("Hello, world!"));
    }
    else {
        cest_set_response_status(rp, 404);
        cest_append_response_body(rp, "Not Found", strlen("Not Found"));
    }
}

int log_request(cest_request_t* rq, cest_response_t* rp, void* data) {
    printf("RQ: %s %s\n",
        cest_request_method_name(cest_get_request_method(rq)),
        cest_get_request_path(rq));
    return 0;
}

int log_response(cest_request_t* rq, cest_response_t* rp, void* data) {
    printf("RP: %i %i bytes\n",
        cest_get_response_status(rp),
        (int)cest_get_response_body_length(rp));
    return 0;
}

int main(int argc, char *argv[])
{
    int port = 0;
    int retval = 0;

    cest_middleware_handler_t* wrapped = cest_create_middleware_handler(handler, NULL);
    cest_append_middleware(wrapped,
        cest_create_middleware(log_request, log_response, NULL, NULL));

    if (argc < 2) {
        perror("Please provide a port number");
        exit(-1);
    }
    port = atoi(argv[1]);

    retval = cest_serve(port, cest_middleware_handler, wrapped);

    cest_destroy_middleware_handler(wrapped);
    return retval;
}
