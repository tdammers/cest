# cest - A Minimalist C Web Framework

# Introduction

cest is a nanoframework. As such, it does at most what a microframework would
do, but more minimalist.

Anyway, cest is kind of an experiment; the goal is to build a complete
standalone web application server, from scratch, in C, with minimal
dependencies. The idea is to see how far this can be taken.

## Features

### Currently implemented:

- a standalone forking HTTP server
- parsing HTTP requests, getting HTTP verb, request path, headers and raw body
- lazy loading HTTP bodies
- streamed reading of HTTP bodies
- writing out HTTP responses, setting response headers
- automatic Content-Length header setting
- compiled "templates" (or rather, an EDSL for constructing well-formed HTML
  documents from within C)

### Not yet implemented, but likely to be in the near future:

- parsing GET parameters
- parsing POST bodies
- parsing paths, and some kind of routing
- cookies and sessions (maybe)
- convenience functions for redirecting and aborting
- JSON support (maybe)
- some shared state between worker processes
- streaming response bodies
- static file serving
- proper support for HEAD and OPTIONS
- a more complete list of HTTP status messages
- a middleware mechanism
- dealing with character encodings / unicode support

### Not yet implemented, needs more thinking:

- implement alternative server strategies: threaded, single-process, prefork?
- internal subrequests
- short-circuiting processing (other languages use exceptions for this)
- catching dead worker processes and serving a 500 instead

### Not yet implemented, less likely to be included:

- SQL database connectivity
- caching
- JSON support

(These are better left to external libraries, although a bit of glue can't
hurt)
