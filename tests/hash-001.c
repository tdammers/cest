#include "cest/internal/hash.h"

#include <stdlib.h>
#include <stdio.h>

int main() {
    const char* test_strings[] = {
        "foobar",
        "asdf",
        "",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "1234wjlfsnljkn2l3nfdspoijvpitn4l3jdnqwlifjdspvojsndofnlgbnljkdjsdpowianjmlfrjsnbgoij;xcs",
        "1",
        NULL
    };
    const char** test_string;
    int failures = 0;

    for (test_string = test_strings; *test_string; ++test_string) {
        int hash1 = _cest_internal_hash(*test_string);
        int hash2 = _cest_internal_hash(*test_string);

        if (hash1 != hash2) {
            printf("Hashes don't match for string \"%s\": %i != %i\n",
                *test_string, hash1, hash2);
            ++failures;
        }
    }

    return failures;
}
