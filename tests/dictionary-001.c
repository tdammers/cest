#include "cest/dictionary.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define assert_equals(a, b, message) \
do { \
    if ((a) != (b)) { \
        printf("Assertion failed: %s\n", (message)); \
        exit(-1); \
    } \
} while (0)

int main() {
    cest_dictionary_t* d = cest_create_dictionary();

    assert_equals(cest_dictionary_lookup(d, "hello"), NULL, "dictionary does not contain key \"hello\" before inserting");
    assert_equals(cest_dictionary_size(d), 0, "dictionary is empty before insert");

    cest_dictionary_insert(d, "hello", "world");
    assert_equals(strcmp(cest_dictionary_lookup(d, "hello"), "world"), 0, "dictionary contains key \"hello\" after inserting");
    assert_equals(cest_dictionary_size(d), 1, "dictionary contains 1 item after insert");
    assert_equals(cest_dictionary_lookup(d, "Hello"), NULL, "keys are case sensititve");
    assert_equals(cest_dictionary_lookup(d, ""), NULL, "empty key does not match nor crash");
    assert_equals(cest_dictionary_lookup(d, NULL), NULL, "NULL key does not match nor crash");

    cest_dictionary_insert(d, "hello", "froobles");
    assert_equals(strcmp(cest_dictionary_lookup(d, "hello"), "froobles"), 0, "dictionary contains key \"hello\" after update");
    assert_equals(cest_dictionary_size(d), 1, "dictionary contains 1 item after update");

    cest_dictionary_remove(d, "hello");
    assert_equals(cest_dictionary_lookup(d, "hello"), NULL, "dictionary does not contain key \"hello\" after deleting");
    assert_equals(cest_dictionary_size(d), 0, "dictionary is empty after delete");
    cest_destroy_dictionary(d);

    d = cest_create_dictionary();
    cest_dictionary_insert(d, "hello", "world");
    cest_dictionary_insert(d, "foo", "bar");
    assert_equals(strcmp(cest_dictionary_lookup(d, "hello"), "world"), 0, "dictionary contains key \"hello\" after inserting");
    assert_equals(strcmp(cest_dictionary_lookup(d, "foo"), "bar"), 0, "dictionary contains key \"hello\" after inserting");
    assert_equals(cest_dictionary_size(d), 2, "dictionary contains 1 item after second insert");
    cest_destroy_dictionary(d);

    return 0;
}
