CC=gcc
LD=gcc
RM=rm -f
CFLAGS=-c -Wall -Werror -Wno-address
# CFLAGS+=-O2
CFLAGS+=-g -ggdb
INCLUDEFLAGS=-I include
CFLAGS+=$(INCLUDEFLAGS)
# LDFLAGS+=-O2
OBJECTSDIR=obj
SOURCEDIR=src
SOURCES=$(wildcard $(SOURCEDIR)/*.c)
OBJECTS=$(patsubst $(SOURCEDIR)/%.c,$(OBJECTSDIR)/%.o,$(SOURCES))
VERSION_MAJOR=0
VERSION_MINOR=0
VERSION=$(VERSION_MAJOR).$(VERSION_MINOR)

PREFIX=/usr/local

EXAMPLEDIR=examples
EXAMPLE_SOURCES=$(wildcard $(EXAMPLEDIR)/*.c)
EXAMPLE_OBJECTS=$(patsubst $(EXAMPLEDIR)/%.c,$(EXAMPLEDIR)/%.o,$(EXAMPLE_SOURCES))
EXAMPLES=$(patsubst $(EXAMPLEDIR)/%.c,$(EXAMPLEDIR)/%,$(EXAMPLE_SOURCES))

TESTDIR=tests
TEST_SOURCES=$(wildcard $(TESTDIR)/*.c)
TEST_OBJECTS=$(patsubst $(TESTDIR)/%.c,$(TESTDIR)/%.o,$(TEST_SOURCES))
TESTS=$(patsubst $(TESTDIR)/%.c,$(TESTDIR)/%,$(TEST_SOURCES))

DOCDIR=docs
DOCSRCDIR=$(DOCDIR)/$(SOURCEDIR)
DOCHTMLDIR=$(DOCDIR)/html
MARKDOWN=markdown
MDDOCS_GENERATED=$(DOCSRCDIR)/index.markdown \
		$(DOCSRCDIR)/000-intro.markdown \
		$(DOCSRCDIR)/999-license.markdown
MDDOCS=$(filter-out $(MDDOCS_GENERATED),$(wildcard $(DOCSRCDIR)/*.markdown))
HTMLDOCS=$(patsubst $(DOCSRCDIR)/%.markdown,$(DOCHTMLDIR)/%.html,$(MDDOCS))
ALLHTMLDOCS=$(patsubst $(DOCSRCDIR)/%.markdown,$(DOCHTMLDIR)/%.html,$(MDDOCS_GENERATED)) $(HTMLDOCS)
HTMLTEMPLATES=$(wildcard $(DOCSRCDIR)/include/*.html)

LIBS=m
LOADLIBES=$(patsubst %,-l%, $(LIBS))
LIBDIR=lib
LIBRARY=$(LIBDIR)/libcest.so
INCLUDESDIR=include/cest
INCLUDES=$(wildcard $(INCLUDESDIR)/*.h)

all: $(LIBRARY)

test: $(TESTS)
	for T in $(TESTS); \
	do LD_LIBRARY_PATH=$(LIBDIR) ./$$T || exit $$?; done

$(LIBRARY): $(LIBDIR) $(SOURCES) depend

$(LIBDIR) $(DOCHTMLDIR) $(OBJECTSDIR):
	mkdir -p $@

examples: $(EXAMPLES)

install: $(LIBRARY)
	install -d $(PREFIX)/lib
	install $(LIBRARY) $(PREFIX)/$(LIBRARY)
	install $(LIBRARY) $(PREFIX)/$(LIBRARY).$(VERSION_MAJOR)
	install $(LIBRARY) $(PREFIX)/$(LIBRARY).$(VERSION)
	install -d $(PREFIX)/$(INCLUDESDIR)
	install $(INCLUDES) $(PREFIX)/$(INCLUDESDIR)
	cd $(PREFIX)/lib; ldconfig

uninstall:
	$(RM) /usr/local/bin/$(EXECUTABLE)
.PHONY: uninstall

clean:
	$(RM) $(OBJECTS) $(EXAMPLE_OBJECTS)
	$(RM) $(MDDOCS_GENERATED)
.PHONY: clean

veryclean: clean
	$(RM) depend
.PHONY: veryclean

distclean: veryclean
	$(RM) $(LIBRARY)
	$(RM) $(EXAMPLES)
	$(RM) $(ALLHTMLDOCS)
.PHONY: distclean

docs: $(ALLHTMLDOCS)

$(DOCSRCDIR)/000-intro.markdown: README.markdown Makefile
	sed -e'1,2d' < $< >$@

$(DOCSRCDIR)/999-license.markdown: LICENSE Makefile
	( \
		echo '# License'; \
		echo ' '; \
		sed -e's/``/\*"/g;s/'\'\''/"\*/g' <LICENSE; \
	) >$@

$(DOCSRCDIR)/index.markdown: $(MDDOCS) $(DOCSRCDIR)/000-intro.markdown Makefile
	( \
		echo '## Contents'; \
		echo ' '; \
		echo '- [License](999-license.html)'; \
		for F in $(sort $(patsubst $(DOCSRCDIR)/%.markdown,%,$(DOCSRCDIR)/000-intro.markdown $(MDDOCS))); \
		do \
			TITLE=$$(sed -e'/^$$/d;2,$$d;s/^\S\+\s\+//' <"$(DOCSRCDIR)/$$F.markdown" ); \
			echo '- ['"$$TITLE"']('"$$F"'.html)'; \
		done; \
	) >$@

$(DOCHTMLDIR)/%.html: $(DOCSRCDIR)/%.markdown Makefile $(HTMLTEMPLATES)
	( \
		cat $(DOCSRCDIR)/include/header.html; \
		$(MARKDOWN) $(MARKDOWNFLAGS) $<; \
		echo "----------\nVersion $(VERSION)" | $(MARKDOWN) $(MARKDOWNFLAGS); \
		cat $(DOCSRCDIR)/include/footer.html \
	) >$@

$(OBJECTSDIR)/%.o: $(SOURCEDIR)/%.c
	$(CC) $(CFLAGS) -fPIC $< -o $@

$(EXAMPLEDIR)/%.o: $(EXAMPLEDIR)/%.c
	$(CC) $(CFLAGS) $< -o $@

$(EXAMPLEDIR)/%: $(EXAMPLEDIR)/%.o $(LIBRARY)
	$(LD) $(LDFLAGS) -L./lib -lcest -o $@ $<

$(TESTDIR)/%.o: $(TESTDIR)/%.c
	$(CC) $(CFLAGS) $< -o $@

$(TESTDIR)/%: $(TESTDIR)/%.o $(LIBRARY)
	$(LD) $(LDFLAGS) -L./lib -lcest -o $@ $<

depend: $(wildcard $(SOURCEDIR)/*.h) $(SOURCES) $(EXAMPLE_SOURCES) $(TEST_SOURCES) Makefile
	$(CC) $(wildcard $(SOURCEDIR)/*.c) $(INCLUDEFLAGS) -MM -MG \
	| sed -e '/^\w/s/^/$(OBJECTSDIR)\//' \
	> depend

$(LIBRARY): $(OBJECTS)
	$(LD) $(LDFLAGS) -shared $(LOADLIBES) -o $@ $(OBJECTS) 

include depend
