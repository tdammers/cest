# Installing

## Getting the sources

Cest is maintained as a git repository at https://bitbucket.org/tdammers/cest.

Check out the sources:

    $ git clone git@bitbucket.org:tdammers/cest
    $ cd cest

## Build requirements

To build the cest library and examples, all you need is a reasonably modern
Linux/Unix install, gcc, GNU make, and libc. Cest does not currently depend on
anything else.

To build the HTML documentation, you'll also need sed, awk, and markdown.

## Compiling & installing

First, edit the makefile to your desires. The defaults should work for a
regular system though.

Then just cd into the directory where you've checked out cest, and do the
usual:

    $ make
    $ sudo make install

By default, cest installs into /usr/local; change the `PREFIX` build variable
in the provided Makefile to point it elsewhere.

To build the HTML documentation:

    $ make docs

To build the examples:

    $ make examples

Once you've done all that, try the ex01 example:

    $ ./examples/ex01 8000

This should start a simple web application on localhost, port 8000. Visit it in
your browser to see if it works.
