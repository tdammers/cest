# Middleware

Middleware is a piece of code that is injected between the client-facing server
and the handler that it wraps; it gets to modify the request before it is
handed to the handler, and to modify the response after the handler has filled
it. Middleware can also abort processing by returning a non-zero value.

A middleware module needs four things: a `before` handler, an `after` handler,
a pointer to its state, and a destructor function to clean up the state when it
is no longer needed.

The `before` handler can be used for things like authentication and other
preliminary checks; the `after` handler is a good place to do things like
output compression, logging, etc.

To wrap a handler with one or more middleware modules, use the
`cest_middleware_handler` function (see `examples/ex_middleware.c` for a usage
example).
