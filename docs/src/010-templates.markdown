# Templates

cest comes with a built-in HTML DSL, implemented as a variadic function
(`cest_write_template`). This function takes a writable `FILE*` stream, and a
variable number of arguments.

## A Simple Example

The following call renders a simple HTML document and demonstrates the basic
syntax.

    cest_write_template(outstream,
      "!", "<!DOCTYPE html>",
      "<html",
        "<head",
          "<title",
            "Hello, world!",
          "</",
        "</",
        "<body",
          "<div",
            "@class", "main-content",
            "<h1",
              "Hello, world!",
            "</",
          "</",
        "</"
      "</",
      NULL);

This renders as:

    <!DOCTYPE html>
    <html>
      <head>
        <title>Hello, world!</title>
      </head>
      <body>
        <div class="main-content">
          <h1>Hello, world!</h1>
        </div>
      </body>
    </html>

## So How Does It Work?

The `cest_write_template` function steps through the arguments until it finds a
NULL value, which is used as a sentinel to signal end-of-document. Depending on
the type of argument, additional tokens may be consumed. The following argument
types are supported:

- `"<..."` opens a tag. All subsequent arguments are parsed in a nested call
  to `cest_write_template` until a matching `"</"` token is encountered. The
  tag name is read from the argument itself. Unlike actual HTML, there is no
  `'>'` character in this tag.
- `"</"` closes a tag. The tag name may be given, but everything after the
  second character in the string is ignored, and no check is performed on the
  tag name - this means that `"<div", "some content", "</table"` is perfectly
  valid, though discouraged.
- `"@..."` adds an attribute to the current tag. The attribute name is read
  from the tag itself, whereas the attribute value (which must be a string) is
  read from the following argument. This argument is only valid inside a tag,
  and only as long as no other arguments have been encountered yet. So this is
  valid: `"<div", "@class", "classname", "some content goes here", "</"`. But
  this is not: `"<div", "some content goes here", "@class", "classname", "</"`.
- `"!", "..."` Interprets the following token as raw HTML. No encoding is
  performed.
- Any other argument is treated as a string literal; it is HTML-encoded and
  written to the output stream. As a special case, if the first character is a
  space (`' '`, decimal 32), it is removed; you can use this feature to escape
  strings that start with one of the special characters above. This means that,
  for example, `" @foobar"` renders as `@foobar`, not as an attribute.
- `"$"` takes two arguments: a *thunk* of type `cest_template_thunk_t`, and a
  *thunk context* of type `void*`, which can be used to pass arbitrary user
  data to the thunk. See below for an example.

## Thunks

A *thunk*, in functional programming, is an expression which hasn't been
evaluated yet; some languages, like Haskell, can pass thunks instead of values,
and have them evaluate only when the value is needed in another computation.
This is known as "lazy evaluation". In cest's template system, thunks are
implemented as function pointers with an extra user data argument. They can be
used to implement things like template inheritance, nested templates, macros,
etc.

Here's a simple example that uses a thunk to write out the same HTML fragment
twice:

    void my_fragment(FILE* f, void*) {
      cest_write_template(f,
        "<div", "@class", "fragment",
          "This is a fragment",
        "</",
        NULL);
    }

    void main_template(FILE* f) {
      cest_write_template(f,
        "!", "<!DOCTYPE html>",
        "<h1", "Say it once:", "</",
        "$", my_fragment, NULL,
        "<h1", "Say it again:", "</",
        "$", my_fragment, NULL,
        NULL);
    }

And it renders as:

    <!DOCTYPE html>
    <h1>Say it once:</h1>
    <div class="fragment">This is a fragment</div>
    <h1>Say it again:</h1>
    <div class="fragment">This is a fragment</div>

Of course, merely repeating things isn't awfully useful; that's where the
context parameter comes in. Example:

    void my_fragment(FILE* f, void* context) {
      char* message = context;
      cest_write_template(f,
        "<h1",
        message,
        "</",
        NULL
      );
    }

    void main_template(FILE* f) {
      cest_write_template(f,
        "!", "<!DOCTYPE html>",
        "$", my_fragment, "Hello",
        "$", my_fragment, "world",
        NULL);
    }

Renders as:

    <!DOCTYPE html>
    <h1>Hello</h1>
    <h1>world</h1>
