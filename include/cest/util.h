#pragma once

#include <stdio.h>

// Some streaming string converters.
// The convention is to read from src, write to dst, and
// return zero on success, nonzero on error.

int cest_html_encode(FILE*, FILE*);
int cest_url_encode(FILE* dst, FILE* src);
int cest_url_decode(FILE* dst, FILE* src);

/**
 * Run a stream converter over an input string, returning the
 * converted stream as a string.
 *
 * Caller must free() the returned string.
 */
char* cest_run_stream_converter(int (*convert)(FILE*, FILE*), const char* src, size_t src_len, size_t *dst_len);

/**
 * Run a converter over an input string, and write the converted
 * result to the specified stream.
 */
int cest_write_stream_converter(int (*convert)(FILE*, FILE*), FILE* dst, const char* src, size_t src_len);
