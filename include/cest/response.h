#pragma once

#include <stdio.h>

struct cest_response;
typedef struct cest_response cest_response_t;

cest_response_t* cest_create_response();
void cest_destroy_response(cest_response_t*);

/**
 * Set the numeric HTTP response status code.
 * Defaults to 200.
 * @param cest_response_t* response
 * @param int status_code
 */
void cest_set_response_status(cest_response_t*, int);

/**
 * Appends or overwrites a response header.
 * @param cest_response_t* response
 * @param const char* header_name
 * @param const char* header_value
 */
void cest_set_response_header(cest_response_t*, const char*, const char*);

/**
 * Gets the current response status code.
 * @param cest_response_t* response
 */
int cest_get_response_status(const cest_response_t*);

/**
 * Clears the response body.
 */
void cest_clear_response_body(cest_response_t* rp);

/**
 * Append a string constant to the current response body.
 *
 * Note that this creates a temporary copy of the string, which will be held in
 * memory until the response is actually sent (or discarded). If you need to
 * send large payloads, it is better to stream content to the response directly
 * by registering a streaming response handler.
 */
void cest_append_response_body(cest_response_t* rq, const char* str, size_t str_len);

/**
 * Overrides the response body with the specified string.
 */
void cest_set_response_body(cest_response_t* rp, const char* str, size_t str_len);

/**
 * Register a stream to be appended to the response body upon sending.
 *
 * If auto_close is nonzero, the stream will be closed upon disposal.
 *
 * f: A readable file handle from which to read
 * content_length: Number of bytes to be sent.
 * auto_close: if nonzero, close the file handle when cleaning up.
 */
void cest_append_response_body_stream(cest_response_t* rq, FILE* f, size_t content_length, int auto_close);

/**
 * Register a custom callback to produce response body data when needed.
 *
 * callback: a function that writes data directly to the response stream
 * cleanup: a function that cleans up after the response has been sent or
 * discarded.
 * data: user-defined data that gets passed to the callback and cleanup
 * functions
 * content_length: number of bytes the callback is supposed to send. It is the
 * responsibility of the caller to make sure that this information is correct.
 */
void cest_append_response_body_callback(cest_response_t* rp, void (*callback)(FILE*, void*), void (*cleanup)(void*), void *data, size_t content_length);

/**
 * Write the response to a file descriptor
 */
void cest_write_response_fd(int fd, const cest_response_t* rp);

/**
 * Write the response to a file handle
 */
void cest_write_response(FILE* f, const cest_response_t* rp);

int cest_get_response_body_length(const cest_response_t* rp);
