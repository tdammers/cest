#pragma once

#include <memory.h>

struct cest_header;
typedef struct cest_header cest_header_t;

struct cest_headers;
typedef struct cest_headers cest_headers_t;

cest_header_t* cest_create_header(const char* name, const char* value);
void cest_destroy_header(cest_header_t*);
const char* cest_get_header_name(const cest_header_t*);
const char* cest_get_header_value(const cest_header_t*);

cest_headers_t* cest_create_headers();
const char* cest_get_header(const cest_headers_t* headers, const char* name);
void cest_set_header(cest_headers_t* headers, const char* name, const char* value);
void cest_add_header(cest_headers_t* headers, const char* name, const char* value);
void cest_destroy_headers(cest_headers_t*);
size_t cest_get_num_headers(const cest_headers_t*);
cest_header_t* const* cest_get_headers(const cest_headers_t*);
