#pragma once

#include "./request.h"
#include "./response.h"

typedef void (*cest_handler_t) (cest_request_t*, cest_response_t*, void*);
