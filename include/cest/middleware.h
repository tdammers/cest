#include <stdlib.h>

#include "./request.h"
#include "./response.h"
#include "./handler.h"

typedef int (*cest_before_handler_f)(cest_request_t*, cest_response_t*, void*);
typedef int (*cest_after_handler_f)(cest_request_t*, cest_response_t*, void*);

typedef struct cest_middleware cest_middleware_t;

cest_middleware_t* cest_create_middleware(
    cest_before_handler_f before,
    cest_after_handler_f after,
    void* state,
    void (*state_dtor)(void*));
void cest_destroy_middleware(cest_middleware_t* middleware);

typedef struct cest_middleware_handler cest_middleware_handler_t;

cest_middleware_handler_t* cest_create_middleware_handler(cest_handler_t handler, void* handler_state);
void cest_destroy_middleware_handler(cest_middleware_handler_t* mwh);
void cest_append_middleware(cest_middleware_handler_t* mwh, cest_middleware_t* m);
void cest_middleware_handler(cest_request_t* rq, cest_response_t* rp, void* data);
