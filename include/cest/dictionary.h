#pragma once

#include <stdlib.h>

typedef struct cest_dictionary cest_dictionary_t;

cest_dictionary_t* cest_create_dictionary();
void cest_destroy_dictionary(cest_dictionary_t*);
void cest_dictionary_insert(cest_dictionary_t*, const char*, const char*);
const char* cest_dictionary_lookup(const cest_dictionary_t*, const char*);
int cest_dictionary_has(const cest_dictionary_t*, const char*);
void cest_dictionary_remove(cest_dictionary_t* d, const char* key);

size_t cest_dictionary_size(const cest_dictionary_t*);
size_t cest_dictionary_bucket_count(const cest_dictionary_t*);
