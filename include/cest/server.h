#pragma once

#include "./handler.h"

int cest_serve(int port, cest_handler_t handler, void* user_state);
