#pragma once

#include <memory.h>
#include <stdio.h>

#include "./headers.h"

struct cest_request;
typedef struct cest_request cest_request_t;

typedef enum cest_request_method {
    CEST_HEAD,
    CEST_GET,
    CEST_OPTIONS,
    CEST_POST,
    CEST_PUT,
    CEST_DELETE
} cest_request_method_t;

cest_request_t* cest_create_request();
void cest_destroy_request(cest_request_t*);

/**
 * Translates request methods to printable strings.
 */
const char* cest_request_method_name(cest_request_method_t);

cest_request_method_t cest_get_request_method(const cest_request_t* rq);
const char* cest_get_request_path(const cest_request_t* rq);

/**
 * Forces the request body to be read.
 */
void cest_load_request_body(cest_request_t* rq);

/**
 * Get the request body read so far.
 * Note that this function does not trigger the body to be read.
 */
const char* cest_get_request_body(const cest_request_t* rq);

/**
 * Get the length of the request body read so far.
 * Note that this function does not trigger the body to be read.
 */
size_t cest_get_request_body_length(const cest_request_t* rq);

/**
 * Read the entire request body and return it.
 * The request body is read at most once per request.
 */
const char* cest_get_full_request_body(cest_request_t* rq);

/**
 * Read the entire request body and return its content length.
 * The request body is read at most once per request.
 */
size_t cest_get_full_request_body_length(cest_request_t* rq);

const cest_headers_t* cest_get_request_headers(const cest_request_t* rq);
const char* cest_get_request_header(const cest_request_t* rq, const char* name);

/**
 * Read a request from the specified file descriptor.
 * Reading the request body is deferred until it is explicitly requested; this
 * means that you should not close the file until the request has been
 * processed completely.
 */
cest_request_t* cest_parse_request_fd(int fd);

/**
 * Read a request from the specified file handle.
 * Reading the request body is deferred until it is explicitly requested; this
 * means that you should not close the file until the request has been
 * processed completely.
 */
cest_request_t* cest_parse_request(FILE* f);

/**
 * Return the value of the GET parameter indicated by `key`, or NULL if the
 * parameter wasn't sent.
 */
const char* cest_get(cest_request_t* rq, const char* key);
