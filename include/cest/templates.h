#pragma once

#include <stdio.h>

typedef void (*cest_template_thunk_t)(FILE* , void*);

void cest_write_template(FILE* f, ...);
