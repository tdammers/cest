#pragma once

#define vector_of(t, struct_name) \
    struct struct_name { \
        t **items; \
        size_t cap; \
        size_t len; \
    }

#define vector_init(t, container) do { \
    (container).items = malloc(sizeof(t)); \
    (container).cap = 1; \
    (container).len = 0; \
} while (0)

#define vector_grow(t, container) do { \
    (container).cap <<= 1; \
    (container).items = realloc((container).items, (container).cap * sizeof(t)); \
} while (0)

#define vector_push(t, container, value) do { \
    if ((container).len >= (container).cap) { \
        vector_grow(t, (container)); \
    } \
    (container).items[(container).len++] = value; \
} while (0)

#define vector_delete(t, container, dtor) do { \
    if (dtor) { \
        size_t i; \
        for (i = 0; i < (container).len; ++i) { \
            (dtor)((container).items[i]); \
        } \
    } \
    free((container).items); \
} while (0)
